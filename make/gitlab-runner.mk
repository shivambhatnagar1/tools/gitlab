# Configuration for gitlab-runner running on MacOS

.PHONY: gitlab-runner-health-check
gitlab-runner-health-check:
	gitlab-runner health-check

.PHONY: gitlab-runner-verify
gitlab-runner-verify:
	gitlab-runner verify

.PHONY: gitlab-runner-run
gitlab-runner-run:
	gitlab-runner run

# Makefile

# Configuration for Make
.ONESHELL:
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

# Project Variables
ENVIRONMENT			    := ${ENVIRONMENT}
MAINTAINERS 			?= "shivam1997bhatnagar@gmail.com"

# Other Targets
include make/gitlab-runner.mk
